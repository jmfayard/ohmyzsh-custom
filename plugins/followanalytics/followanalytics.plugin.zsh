# Add yourself some shortcuts to projects you often work on
# Example:
#
# brainstormr=/Users/robbyrussell/Projects/development/planetargon/brainstormr
#
export DEV=$HOME/Dev
export ANDROID_HOME="/Applications/DevApps/Android Studio.app/sdk"
export fa_SDK=$DEV/sdk_android_v2
export fa_Android_Test=$DEV/followapps-android-test-app

function fa_build_sdk() {
	echo "Starting to build the FollowAnalytics SDK"
	cd $fa_SDK
	echo gradle clean build check makeJar publishToMavenLocal
}

function fa_build_androidtest() {
	echo "Starting to build the Android Test App"
	cd $fa_Android_Test
	echo gradle  -PtestMode clean generateManifestBeforeBuild build
}
